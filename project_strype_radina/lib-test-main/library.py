import random
import string
from datetime import date, datetime


class Library():
    def __init__(self, name, location):
        self._name = name
        self._location = location
        self.all_books = []
        self.fantasy_category = []
        self.horror_category = []
        self.romance_category = []
        self.selfhelp_category = []
        self.programming_tech_category = []
        self.rented_books = []
        self.membership = None

    def __str__(self):
        return f"Welcome to the Library {self._name}.Enjoy your stay!"

    def create_new_book(self, book_name, author, genre, date_created):
        def generate_ISBN():
            lenght = 13
            isbn = ''.join(random.choices(
                string.ascii_letters+string.digits, k=lenght))
            return isbn.upper()
        ISBN = generate_ISBN()
        new_book = {"ISBN": ISBN, "book_name": book_name, "author": author,
                    "genre": genre,
                    "creation_date": date_created,
                    "rented": False, }
        self.all_books.append(new_book)

    def list_all_books(self):
        print("Current list of all books available: ")
        for item in self.all_books:
            print(item)

    def list_certain_category(self, category):
        category = category.lower()
        if category == "romance":
            print(f"Current books in {category} category: ")
            for item in self.romance_category:
                print(item)
        elif category == "selfhelp":
            print(f"Current books in {category} category: ")
            for item in self.selfhelp_category:
                print(item)
        elif category == "horror":
            print(f"Current books in {category} category: ")
            for item in self.horror_category:
                print(item)
        elif category == "fantasy":
            print(f"Current books in {category} category: ")
            for item in self.fantasy_category:
                print(item)
        elif category == "programming/tech":
            print(f"Current books in {category} category: ")
            for item in self.programming_tech_category:
                print(item)
        else:
            print("Invalid category")

    # def list_romance_books(self):
    #     print("Current books in romance category")
    #     for item in self.romance_category:
    #         print(item)

    def delete_book(self, name):
        for item in self.all_books:
            if item["book_name"] == name:
                self.all_books.remove(item)

    def add_to_genre(self, genre):
        for item in self.all_books:
            if genre == "fantasy":
                if item["genre"] != genre:
                    continue
                else:
                    self.fantasy_category.append(item)
            elif genre == "horror":
                if item["genre"] != genre:
                    continue
                else:
                    self.horror_category.append(item)
            elif genre == "romance":
                if item["genre"] != genre:
                    continue
                else:
                    self.romance_category.append(item)
            elif genre == "selfhelp":
                if item["genre"] != genre:
                    continue
                else:
                    self.selfhelp_category.append(item)
            elif genre == "programming/tech":
                if item["genre"] != genre:
                    continue
                else:
                    self.programming_tech_category.append(item)

    def rent_book(self, book_name, user_name, user_surname):
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        for book in self.all_books:
            if book["book_name"] == book_name:
                book["rented"] = True
                book["rented_by"] = user_name + " " + user_surname
                book["rented_date"] = dt_string
                if book["book_name"] != book_name:
                    continue
                else:
                    self.rented_books.append(book)
                    self.all_books.remove(book)

    def list_rented_books(self):
        print("List of current rented book:")
        for book in self.rented_books:
            print(book)

    def save_to_file_romance_books(self):
        with open("romance_books.txt", "a") as file:
            for book in self.romance_category:
                for key in book:
                    file.write(f"{key}: {book[key]}\n")
                    file.write("")

    def save_to_file_horror_books(self):
        with open("horror_books.txt", "a") as file:
            for book in self.horror_category:
                for key in book:
                    file.write(f"{key}: {book[key]}\n")
                    file.write("")

    def save_to_file_fantasy_books(self):
        with open("fantasy_books.txt", "a") as file:
            for book in self.fantasy_category:
                for key in book:
                    file.write(f"{key}: {book[key]}\n")
                    file.write("")

    def save_to_file_selfhelp_books(self):
        with open("selfhelp_books.txt", "a") as file:
            for book in self.selfhelp_category:
                for key in book:
                    file.write(f"{key}: {book[key]}\n")
                    file.write("")

    def save_to_file_programming_tech(self):
        with open("programming/tech.txt", "a") as file:
            for book in self.programming_tech_category:
                for key in book:
                    file.write(f"{key}: {book[key]}\n")
                    file.write("")

    def save_to_file_all_books(self):
        with open("all_books.txt", "a") as file:
            for book in self.all_books:
                for key in book:
                    file.write(f"{key}: {book[key]}\n")
                    file.write("")

    def save_to_rented_books(self):
        with open("rented_books.txt", "a") as file:
            for book in self.rented_books:
                for key in book:
                    file.write(f"{key}: {book[key]}\n")
                    file.write("")

        # def test(self):
        #     for book in self.all_books:
        #         for key in book:
        #             print(f"{key} : {book[key]}")
