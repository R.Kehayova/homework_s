from library import Library
from user import User

if __name__ == "__main__":

    new_library = Library("Saint Jonson", "England")
    new_user = User("Radina", "Kehayova", 21)
    new_user2 = User("Dimo", "Dimov", 21)

    print(new_library)
    new_library.create_new_book("Together & Forever",
                                "Rob Percy", "romance", 1999)
    new_library.create_new_book("Keep going Bob!",
                                "William West", "fantasy", 2005)
    new_library.create_new_book("IT final chapter",
                                "Steven King", "horror", 2016)
    new_library.create_new_book("Learn Python3 the hard way",
                                "Zed Shaw", "programming/tech", 2013)
    # new_library.delete_book("XD")
    new_library.list_all_books()
    # new_library.add_to_genre("fantasy")
    # new_library.add_to_genre("horror")
    # new_library.add_to_genre("romance")
    # new_library.add_to_genre("selfhelp")
    # # new_library.list_certain_category("romancE")

    new_library.rent_book("Learn Python3 the hard way",
                          new_user._name, new_user._surname)
    new_library.rent_book("IT final chapter",
                          new_user2._name, new_user2._surname)
    new_library.list_all_books()
    # print(new_library.rented_books)
    # print(new_library.all_books)
    # new_library.list_all_books()
    print(50*"-")
    new_library.list_rented_books()

    # new_library.save_to_file_romance_books()
    # new_library.save_to_file_all_books()
    new_library.save_to_rented_books()
