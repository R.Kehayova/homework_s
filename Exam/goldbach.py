def goldbach(n):
    my_list_prime = []
    my_list_result = []
    my_temp_result = []
    for num in range(2 , n - 1):  
        if num > 1:  
            for i in range(2, num ):  
                if (num % i) == 0:  
                    break  
            else:  
                my_list_prime.append(num)
    for digits in my_list_prime: 
        if digits <= n / 2:
            temp_num = n - digits
            if temp_num in my_list_prime:
                my_temp_result.append(digits)
                my_temp_result.append(temp_num )
                my_temp_result = tuple(my_temp_result)
                my_list_result.append(my_temp_result)
                my_temp_result = []
                temp_num = 0
    return my_list_result
print(goldbach(4))
print(goldbach(6))
print(goldbach(8))
print(goldbach(10))
print(goldbach(100))