class Player:      
    def __init__(self, name , health , list_weapons):
        self.__name = name
        self.__health = health
        self.__list_weapons = list_weapons

    def get__name(self):
        return self.__name

    def get__health(self):
        return self.__health

    def get__list_weapons(self):
        return self.__list_weapons

    def set_name (self , name):
        self.__name = name

    def set_health(self , health):
        self.__health = health

    def set_list_weapons(self , list_weapons):
        self.__list_weapons = list_weapons

    def take_damage(self , damage):
        self.set_health(self.get__health() - damage)
        

    def shoot(self , weapon , enemy):
        enemy.take_damage(weapon.fire())

    def reload(self , weapon):
        weapon.reload()

class Weapon:
    def __init__(self, damage , clip_size , clip_bullets):
        self.damage = damage
        self.clip_size = clip_size
        self.clip_bullets = clip_bullets

    def reload(self):
        self.clip_bullets = self.clip_size

    def fire(self):
        if self.clip_bullets > 0:
            self.clip_bullets -= 1      
            return self.damage
        else:
            self.reload()
class ArmoredPlayer(Player):
    def __init__(self, name , health , list_weapons , armor):
        super().__init__(name , health , list_weapons)
        self.__armor = armor
    def get__armor(self):
        return self.__armor
    def set_armor(self , armor):
        self.__armor = armor
        if armor < 0 :
            self.__armor = 0
    def take_damage(self, damage):
        if self.__armor < damage:
            damage_health = damage - self.__armor 
            self.__armor = 0
            super().take_damage(damage_health)
        else:
            self.set_armor(self.get__armor() - damage)

    
    
list_weapons = []
list_weapons.append(Weapon(199 , 30 , 20))
list_weapons.append(Weapon(10 , 15 , 5))
list_weapons.append(Weapon(50 , 20 , 20))
list_weapons.append(Weapon(70 , 5 , 2))
Ivan = Player("Ivan" , 100 , list_weapons )
Dimo = Player("Dimo" , 100 , list_weapons )
Petko = ArmoredPlayer("Petko" , 100 , list_weapons , 100)
print(Petko.get__armor())
print(Petko.get__health())
Dimo.shoot(Dimo.get__list_weapons()[0] , Petko)
print(Petko.get__armor())
print(Petko.get__health())