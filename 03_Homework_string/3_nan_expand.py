def nan_expand(num):
    sum = ""
    if num == 0:
        return "''"
    else:
        sum = "Not a " * num
    return sum + "NaN"

print(nan_expand(0))
print(nan_expand(1))
print(nan_expand(2))
print(nan_expand(3))