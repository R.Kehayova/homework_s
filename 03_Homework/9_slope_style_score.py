def slope_style_score(scores):
    scores.sort()
    del scores[0]
    del scores[-1]
    element = len(scores)
    score= sum(scores) / element
    score_round = round(score, 2)
    return score_round
print(slope_style_score([94, 95, 95, 95, 90]))
print(slope_style_score([60, 70, 80, 90, 100]))
print(slope_style_score([96, 95.5, 93, 89, 92]))