def count_substrings(haystack, needle):
    sum = 0
    counter_word = 0
    for letter in haystack:
        if letter == needle[counter_word]:
            if counter_word < len(needle)-1:
                counter_word += 1
            else:
                counter_word = 0
                sum += 1        
    return sum

print(count_substrings("This is a test string", "is"))
print(count_substrings("babababa", "baba"))
print(count_substrings("Python is an awesome language to program in!", "o"))
print(count_substrings("We have nothing in common!", "really?"))
print(count_substrings("this is this and that is this", "this"))