def contains_digits(number, digits):
    my_list = []
    while number > 0:
        num = number % 10 
        my_list.append(num)
        number = number // 10
    for dig in range(len(digits)):
        if digits[dig] not in my_list:
            return False
    return True

print(contains_digits(402123, [0, 3, 4]))
print(contains_digits(666, [6,4]))
print(contains_digits(123456789, [1,2,3,0]))
print(contains_digits(456, [ ]))