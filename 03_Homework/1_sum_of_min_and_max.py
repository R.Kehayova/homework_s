def sum_of_min_and_max(arr):
    if len(arr) == 1:
        return arr
    min_num = arr[0]
    max_num = arr[1]
    if min_num > max_num:
        temp_max = max_num
        max_num = min_num 
        min_num = temp_max
    for digits in arr:
        if min_num > digits:
            min_num = digits
        if max_num < digits:
            max_num = digits
    return min_num + max_num
print(sum_of_min_and_max([1,2,3,4,5,6,8,9]))
print(sum_of_min_and_max([-10,5,10,100]))
print(sum_of_min_and_max([1]))
#