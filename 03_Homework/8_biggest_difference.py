def biggest_difference(arr):
    my_list = list(arr)
    my_list.sort()
    s_number = my_list[0]
    l_number = my_list[-1]
    result = s_number - l_number
    return result
print(biggest_difference([1,2]))
print(biggest_difference([1,2,3,4,5]))
print(biggest_difference([-10, -9, -1]))
print(biggest_difference(range(100)))