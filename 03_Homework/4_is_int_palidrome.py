def is_int_palindrome(n):
    my_list = []
    my_list_r = []
    while n > 0:
        digit = n % 10 
        my_list.append(digit)
        my_list_r.append(digit)
        n = n // 10
    my_list_r.reverse()    
    if my_list == my_list_r:
        return True
    else:
        return False
print(is_int_palindrome(1))
print(is_int_palindrome(42))
print(is_int_palindrome(100001))
print(is_int_palindrome(999))
print(is_int_palindrome(123))