def contains_digit(number, digit):
    my_list = []
    while number > 0:
        num = number % 10 
        my_list.append(num)
        number = number // 10
    if digit in my_list:
        return True
    else:
        return False
print(contains_digit(123, 4))
print(contains_digit(42, 2))
print(contains_digit(1000, 0))
print(contains_digit(12346789, 5))
