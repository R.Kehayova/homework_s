def reading_func (file):
    with open(file , 'r') as reader:
        line = reader.readline()
        while line != '':
            print(line, end = '')
            line = reader.readline()
        return line
print(reading_func('text_file.txt'))