def to_number(digit_list):
    number = 0
    for digits in digit_list:
        if digits > 9:
            number *=  100
        else:
            number *=  10
        number += digits
    return number

def fib_number(n):
    current_num = 1
    priv_num = 0
    my_list = []
    for digit in range (0 , n):
        next_num = current_num + priv_num
        my_list.append(current_num)
        priv_num = current_num
        current_num = next_num  
    return my_list
print  (to_number(fib_number(3)))
print  (to_number(fib_number(10)))