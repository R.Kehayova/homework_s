
def is_number_balanced(n):
    my_list = []
    left_side = 0
    right_side = 0
    while n > 0:
        digit = n % 10 
        my_list.append(digit)
        n = n // 10
    my_list.reverse()
    print(my_list)
    med = len(my_list) // 2
    for digits in range(0 , len(my_list)):
        if digits < med:
            left_side += my_list[digits]
            print(left_side)
        else:
            right_side += my_list[digits]
            print(right_side)
    return left_side == right_side
        

print(is_number_balanced(3122))