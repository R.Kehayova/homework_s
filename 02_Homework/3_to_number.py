def to_number(digit_list):
    number = 0
    for digits in digit_list:
        if digits > 9:
            number *=  100
        else:
            number *=  10
        number += digits
    return number
print (to_number([1,2,3]))
print (to_number([9,9,9,9,9]))
print (to_number([1,2,3,0,2,3]))
print (to_number([21, 2, 33]))