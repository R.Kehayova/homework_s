def to_digits(n):
    my_list = []
    while n > 0:
        digit = n % 10 
        my_list.append(digit)
        n = n // 10
    my_list.reverse()
    return my_list
print (to_digits(123))
print (to_digits(99999))
print (to_digits(123023))